# open_api

### Features
- Controller
- Routes

### Quick Start

Install package modules
        
    npm install

Environment configuration
        
    cp .env.example .env

Run application
    
    npm start

## License

[MIT](LICENSE)