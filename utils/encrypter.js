"use strict";

const fs = require("fs");
const { generateKeyPairSync } = require("crypto");
const forge = require("node-forge");
const optionFs = { recursive: true };

const generatePrivateKey = (req, res) => {
  const { privateKey } = generateKeyPairSync("rsa", {
    modulusLength: 2048,
    privateKeyEncoding: {
      type: "pkcs1",
      format: "pem"
    }
  });

  let dir = __dirname + "/../storage";

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, optionFs);
  }

  fs.appendFileSync(dir + "/private.pem", privateKey);

  return req.output(
    req,
    res,
    { error: false, message: "success generate private key" },
    "info",
    200
  );
};

const read = (pathname, password, callback) => {
  let privateKey;
  let p12Buffer = fs.readFileSync(pathname);

  if (password != "") {
    privateKey = forge.pki.decryptRsaPrivateKey(p12Buffer, password);
  } else {
    privateKey = forge.pki.decryptRsaPrivateKey(p12Buffer);
  }

  let publicKey = forge.pki.setRsaPublicKey(privateKey.n, privateKey.e);
  let output = {
    code: 0,
    privateKey: forge.pki.privateKeyToPem(privateKey),
    publicKey: forge.pki.publicKeyToPem(publicKey)
  };

  callback(null, output);
};

module.exports = {
  generatePrivateKey,
  read
};
