module.exports = [
  {
    module: "Login",
    host: "http://localhost:3002",
    endpoint: "/api/v1/login"
  },
  {
    module: "Register",
    host: "http://localhost:3002",
    endpoint: "/api/v1/register"
  },
  {
    module: "Users",
    host: "http://localhost:3002",
    endpoint: "/api/v1/users"
  }
];
