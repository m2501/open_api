const unirest = require("unirest");

const post = (url, body, options, callback) => {
  let headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };

  if (options.headers.authorization) {
    headers["Authorization"] = "Bearer " + options.headers.authorization;
  }

  unirest
    .post(url)
    .headers(headers)
    .send(body)
    .then(response => {
      callback(null, response.body);
    });
};

module.exports = { post };
