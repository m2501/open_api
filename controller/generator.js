"use strict";

const encypter = require("../utils/encrypter");
const RSA_SHA = require("../utils/RSA-SHA");
const _ = require("lodash");
const hitter = require("../utils/hitter");
const modules = require("../utils/modules");

const encryptRequest = (req, res) => {
  encypter.read(
    `${__dirname}/../storage/private.pem`,
    "halohalo",
    (err, output) => {
      console.log(output.publicKey);
      const encypted = RSA_SHA.encryptRSA(
        JSON.stringify(req.body),
        output.publicKey
      );

      return req.output(
        req,
        res,
        {
          error: false,
          data: encypted
        },
        "info",
        200
      );
    }
  );
};

const decryptRequest = (req, res) => {
  encypter.read(
    `${__dirname}/../storage/private.pem`,
    "halohalo",
    (err, output) => {
      const decrypted = RSA_SHA.decryptRSA(req.body.data, output.privateKey);

      return req.output(
        req,
        res,
        {
          error: false,
          data: JSON.parse(decrypted)
        },
        "info",
        200
      );
    }
  );
};

const createSignature = (req, res) => {
  encypter.read(
    `${__dirname}/../storage/privateSign.pem`,
    "halohalo",
    (err, outputSign) => {
      const signed = RSA_SHA.signSHA(
        JSON.stringify(req.body.request),
        outputSign.privateKey
      );

      return req.output(
        req,
        res,
        {
          error: false,
          data: signed
        },
        "info",
        200
      );
    }
  );
};

const verifyRequest = (req, res) => {
  encypter.read(
    `${__dirname}/../storage/private.pem`,
    "halohalo",
    (err, output) => {
      let data = RSA_SHA.decryptRSA(req.body.data, output.privateKey);
      data = JSON.parse(data);

      encypter.read(
        `${__dirname}/../storage/privateSign.pem`,
        "halohalosign",
        (err, outputSign) => {
          const verified = RSA_SHA.verifySHA(
            JSON.stringify(data.request),
            data.signature,
            outputSign.publicKey
          );

          if (verified) {
            let findModule = _.find(modules, { module: data.module });

            if (!findModule) {
              return req.output(
                req,
                res,
                {
                  error: true,
                  message: "module not found",
                  data: {}
                },
                "info",
                500
              );
            } else {
              let options = {};
              // console.log(data)
              if (typeof data.authorization !== undefined) {
                options.headers = {
                  authorization: data.authorization
                };
              }

              hitter.post(
                findModule.host + findModule.endpoint,
                data.request,
                options,
                (err, result) => {
                  if (err)
                    return req.output(
                      req,
                      res,
                      {
                        error: true,
                        message: "failed",
                        data: err
                      },
                      "info",
                      500
                    );

                  return req.output(
                    req,
                    res,
                    {
                      error: false,
                      message: "success",
                      data: result
                    },
                    "info",
                    200
                  );
                }
              );
            }
          } else {
            return req.output(
              req,
              res,
              {
                error: true,
                message: "unverified",
                data: {}
              },
              "info",
              500
            );
          }
        }
      );
    }
  );
};

module.exports = {
  encryptRequest,
  decryptRequest,
  createSignature,
  verifyRequest
};
