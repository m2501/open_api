"use-strict";

const express = require("express");
const router = express.Router();
const encrypter = require("../utils/encrypter");
const generator = require("../controller/generator");

module.exports = app => {
  // utils
  router.get("/generate/private-key", encrypter.generatePrivateKey);

  router.post("/encrypt-request", generator.encryptRequest);
  router.post("/decrypt-request", generator.decryptRequest);
  router.post("/create-signature", generator.createSignature);
  router.post("/verify-request", generator.verifyRequest);

  app.use("/api", router);
};
